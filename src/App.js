import { Fragment, useState } from 'react';
import AddUser from './components/Users/AddUser';
import UserList from './components/Users/UserList';

const App = () => {
  const [userList, setUserList] = useState([]);

  const addUserHandler = (username, age) => {
    setUserList((previousUserList) => {
      return [
        ...previousUserList,
        { name: username, age: age, id: Math.random().toString() },
      ];
    });
  };

  return (
    <Fragment>
      <AddUser onAddUser={addUserHandler} />
      <UserList users={userList} />
    </Fragment>
  );
};

export default App;
